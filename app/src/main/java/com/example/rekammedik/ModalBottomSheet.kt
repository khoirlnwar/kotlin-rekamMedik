package com.example.rekammedik

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.example.rekammedik.Constants.COLLECTIONS_NAME
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.util.*

class ModalBottomSheet : BottomSheetDialogFragment() {

    private var etNamaPasien: EditText? = null
    private var etUsiaPasien: EditText? = null
    private var etRekamMedis: EditText? = null

    private var selectedDate: TextView? = null
    private var btnSelectDate: Button? = null

    private var checkedRadioButtonId: Int? = null
    private var btnSave: Button? = null
    private var radioGroup: RadioGroup? = null

    private var progressDialog: ProgressDialog? = null
    private val viewModel: DashboardViewModel by activityViewModels()

    private var timestamp: Long? = null

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v: View = inflater.inflate(R.layout.bottom_sheet_layout, container, false)

        etNamaPasien = v.findViewById(R.id.et_nama_pasien)
        etUsiaPasien = v.findViewById(R.id.et_usia_pasien)
        etRekamMedis = v.findViewById(R.id.et_rekam_medis)

        btnSelectDate = v.findViewById(R.id.btn_select_date)
        selectedDate = v.findViewById(R.id.tv_date)

        radioGroup = v.findViewById(R.id.radio_group_status)

        checkedRadioButtonId = radioGroup?.checkedRadioButtonId

        progressDialog = ProgressDialog(v.context)

        btnSelectDate?.setOnClickListener {

            val calendar = Calendar.getInstance()
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)

            val datepickerdialog = DatePickerDialog(
                v.context,
                { view, sYear, sMonth, sDay ->
                    selectedDate?.text = "$sDay/${sMonth + 1}/$sYear"

                    calendar.set(sYear, sMonth, sDay)
                    timestamp = calendar.timeInMillis

                    // Log.d(TAG, "onCreateView: ts: " + timestamp)
                },
                year,
                month,
                dayOfMonth
            )

            datepickerdialog.show()
        }

        btnSave = v.findViewById(R.id.btn_save)
        btnSave?.setOnClickListener {
            save()
        }

        return v
    }

    companion object {
        const val TAG = "ModalBottomSheet"
    }

    private fun save() {
        progressDialog?.setTitle("Mohon tunggu")
        progressDialog?.setMessage("Data rekam medis sedang disimpan")
        progressDialog?.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog?.show()
        progressDialog?.setCancelable(false)

        val db = Firebase.firestore

        var statusAktif: String? = null

        checkedRadioButtonId = radioGroup?.checkedRadioButtonId
        when (checkedRadioButtonId) {
            R.id.radio_button_aktif -> statusAktif = "aktif"
            else -> statusAktif = "Nonaktif"
        }

        val namaPasien = etNamaPasien?.text.toString()
        val usiaPasien = etUsiaPasien?.text.toString()
        val rekamMedis = etRekamMedis?.text.toString()

        val date = selectedDate?.text.toString()

        if (namaPasien.isNotEmpty() && usiaPasien.isNotEmpty() && rekamMedis.isNotEmpty()) {
            val data = hashMapOf(
                "nama_pasien" to namaPasien,
                "usia_pasien" to usiaPasien,
                "rekam_medis_pasien" to rekamMedis,
                "tanggal" to timestamp,
                "status" to statusAktif
            )

            db.collection(COLLECTIONS_NAME)
                .add(data)
                .addOnSuccessListener {
                    Toast.makeText(context, "Berhasil disimpan", Toast.LENGTH_SHORT).show()
                    viewModel.loadDocuments()
                    progressDialog?.dismiss()
                    dismiss()
                }
                .addOnFailureListener {
                    Log.w(TAG, "save: Error adding document" + it.message)
                    progressDialog?.dismiss()
                }
        } else {
            Toast.makeText(context, "Gagal disimpan, ada data yang kosong", Toast.LENGTH_SHORT)
                .show()
            progressDialog?.dismiss()
        }


    }

}