package com.example.rekammedik

data class RekamMedisData(
    val id: String,
    val nama: String,
    val usia: String,
    val deskripsi: String,
    val status: String,
    val tanggal: String,
    val daftarGambar: ArrayList<String>,
)
