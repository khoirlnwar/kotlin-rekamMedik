package com.example.rekammedik

import ImageAdapter
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.ContentValues.TAG
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.icu.text.DateFormatSymbols
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64.encodeToString
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.rekammedik.Constants.COLLECTIONS_NAME
import com.example.rekammedik.Constants.REQUEST_CODE
import com.example.rekammedik.databinding.ActivityCreateRecordBinding
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.io.ByteArrayOutputStream
import java.util.*
import kotlin.collections.ArrayList

class CreateRecordActivity : AppCompatActivity() {

    private var binding: ActivityCreateRecordBinding? = null
    private val viewModel: CreateRecordActivityViewModel by viewModels()
    private val dashboardViewModel: DashboardViewModel by viewModels()

    private var timestamp: Long? = null
    private var daftarImageBmp: ArrayList<Bitmap>? = ArrayList()

    private var progressDialog: ProgressDialog? = null
    private var rmedis: RekamMedisData? = null

    private var isUpdate: Boolean = false
    private var documentId: String? = null

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCreateRecordBinding.inflate(layoutInflater)
        val view = binding?.root

        setContentView(view)

        val intent: Intent = intent
        documentId = intent.getStringExtra("ID_ITEM")

        initViewModel()

        if (documentId != null) {
            viewModel.loadDetailDocuments(documentId!!)
            isUpdate = true

            binding?.btnHapus?.setOnClickListener {
                hapusData()
            }

        } else {
            isUpdate = false
            binding?.btnHapus?.visibility = View.INVISIBLE
        }

        progressDialog = ProgressDialog(this)

        binding?.btnSelectDate?.setOnClickListener {
            pickDate()
        }

        binding?.btnOpenGallery?.setOnClickListener {
            pickMultiImageFromGallery()
        }

        binding?.btnSave?.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (!isUpdate) save() else update()
            }
        }

        binding?.rvImages?.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding?.rvImages?.setHasFixedSize(true)
    }


    private fun initViewModel() {

        // create observer
        val daftarImageUriObs = Observer<List<Bitmap>> {
            daftarImageBmp = viewModel.getDaftarImageBmp().value
            daftarImageBmp?.let {
                binding?.rvImages?.adapter = ImageAdapter(daftarImageBmp!!)
            }
        }

        val detailRekamMedisObs = Observer<RekamMedisData> {
            rmedis = viewModel.getDetailRekamMedis().value!!

            binding?.etNamaPasien?.setText(rmedis!!.nama)
            binding?.etUsiaPasien?.setText(rmedis!!.usia)
            binding?.etRekamMedis?.setText(rmedis!!.deskripsi)
            binding?.tvDate?.text = rmedis!!.tanggal

            val splitted = rmedis!!.tanggal.split("/")

            val month: Int = getMonthIndex(splitted[1])
            val year: Int = splitted[2].toIntOrNull()!!
            val day: Int = splitted[0].toIntOrNull()!!

            val calendar = Calendar.getInstance()
            calendar.set(year, month, day)

            timestamp = calendar.timeInMillis

            if (rmedis!!.status == "Aktif")
                binding?.radioButtonAktif?.isChecked = true
            else binding?.radioButtonTidakAktif?.isChecked = true

            val arrayBmp: ArrayList<Bitmap> = ArrayList<Bitmap>()
            for (element in 0 until rmedis!!.daftarGambar.size) {
                arrayBmp.add(base64ToBitmap(rmedis!!.daftarGambar[element]))
            }

            viewModel.setDaftarImageBmp(arrayBmp)

        }

        // start observing
        viewModel.daftarImageBmp.observe(this, daftarImageUriObs)
        viewModel.detailRekamMedis.observe(this, detailRekamMedisObs)
    }

    private fun pDialogStart() {
        progressDialog?.setTitle("Mohon tunggu")
        progressDialog?.setMessage("Data rekam medis sedang disimpan")
        progressDialog?.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog?.show()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun save() {
        if (validation()) {

            pDialogStart()
            val db = Firebase.firestore

            val statusAktif = when (binding?.radioGroupStatus?.checkedRadioButtonId!!) {
                R.id.radio_button_aktif -> "Aktif"
                else -> "Nonaktif"
            }

            val listBase64: ArrayList<String> = ArrayList()

            viewModel.getDaftarImageBmp().value?.forEach {
                val base64 = bitmapToBase64(it)
                listBase64.add(base64!!)
            }

            val data = hashMapOf(
                "nama" to binding?.etNamaPasien?.text.toString(),
                "usia" to binding?.etUsiaPasien?.text.toString(),
                "informasi" to binding?.etRekamMedis?.text.toString(),
                "tanggal" to binding?.tvDate?.text.toString(),
                "status" to statusAktif,
                "images" to listBase64,
            )

            db.collection(COLLECTIONS_NAME)
                .add(data)
                .addOnSuccessListener {
                    shToast("data berhasil disimpan")

                    dashboardViewModel.loadDocuments()
                    progressDialog?.dismiss()
                    finish()
                }
                .addOnFailureListener {
                    shToast("Gagal simpan data" + it.message)
                    progressDialog?.dismiss()
                }

        }
    }

    private fun hapusData() {
        pDialogStart()

        val db = Firebase.firestore
        // db.collection(COLLECTIONS_NAME).document(documentId)
        documentId?.let {
            db
                .collection(COLLECTIONS_NAME)
                .document(documentId!!)
                .delete()
                .addOnSuccessListener {
                    shToast("Data berhasil dihapus")

                    dashboardViewModel.loadDocuments()
                    finish()
                }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun update() {
        if (validation()) {
            pDialogStart()

            val db = Firebase.firestore

            val statusAktif = when (binding?.radioGroupStatus?.checkedRadioButtonId) {
                R.id.radio_button_aktif -> "Aktif"
                else -> "Nonaktif"
            }

            val listBase64: ArrayList<String> = ArrayList()

            viewModel.getDaftarImageBmp().value?.forEach {
                val base64 = bitmapToBase64(it)
                listBase64.add(base64!!)
            }

            Log.e(TAG, "update: updating ...")

            val dataToSend = hashMapOf(
                "nama" to binding?.etNamaPasien?.text.toString(),
                "usia" to binding?.etUsiaPasien?.text.toString(),
                "informasi" to binding?.etRekamMedis?.text.toString(),
                "tanggal" to binding?.tvDate?.text.toString(),
                "status" to statusAktif,
                "images" to listBase64,
            )


            Log.d(TAG, "update: doc id $documentId")
            documentId?.let {
                db
                    .collection(COLLECTIONS_NAME)
                    .document(documentId!!)
                    .set(dataToSend)
                    .addOnSuccessListener {
                        Log.d(TAG, "update: success")
                        shToast("data berhasil disimpan")

                        dashboardViewModel.loadDocuments()
                        progressDialog?.dismiss()
                        finish()
                    }
                    .addOnFailureListener {
                        Log.e(TAG, "update: failed " + it.message)
                        shToast("Gagal simpan data" + it.message)
                        progressDialog?.dismiss()
                    }
            }
        }
    }

    private fun shToast(message: String) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


    private fun validation(): Boolean {
        if (binding?.etNamaPasien?.text?.length!! < 2) {
            shToast("Nama pasien tidak boleh kurang dari 2 karakter")
            return false
        }

        if (binding?.etUsiaPasien?.text?.isEmpty() == true) {
            shToast("Usia pasien tidak boleh kosong")
            return false
        }

        if (binding?.etRekamMedis?.text?.isEmpty() == true) {
            shToast("Rekam medis tidak boleh kosong")
            return false
        }

        if (timestamp == null) {
            shToast("Tanggal tidak boleh kosong")
            return false
        }


        if (viewModel.daftarImageBmp.value!!.isEmpty()) {
            shToast("Gambar tidak boleh kosong")
            return false
        }

        return true

    }


    @RequiresApi(Build.VERSION_CODES.N)
    private fun pickDate() {
        val calendar = Calendar.getInstance()
        val nYear = calendar.get(Calendar.YEAR)
        val nMonth = calendar.get(Calendar.MONTH)
        val nDay = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            this,
            { view, year, month, day ->
                binding?.tvDate?.text = "$day/${getMonthName(month)}/$year"
                calendar.set(year, month, day)

                timestamp = calendar.timeInMillis
            },
            nYear,
            nMonth,
            nDay
        )

        datePickerDialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun getMonthName(month: Int): String {
        return DateFormatSymbols().months[month - 1]
    }

    private fun getMonthIndex(month: String): Int {
        return when (month) {
            "January" -> 1
            "February" -> 2
            "March" -> 3
            "April" -> 4
            "May" -> 5
            "June" -> 6
            "July" -> 7
            "August" -> 8
            "September" -> 9
            "October" -> 10
            "November" -> 11
            else -> 12
        }
    }

    private fun pickMultiImageFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, REQUEST_CODE)
    }

    private fun base64ToBitmap(base64: String): Bitmap {
        val imageBytes = android.util.Base64.decode(base64, 0)
        val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

        return image
    }

    private fun imageToBitmap(uri: Uri): Bitmap {
        return MediaStore.Images.Media.getBitmap(contentResolver, uri);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun bitmapToBase64(bitmap: Bitmap): String? {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray: ByteArray = byteArrayOutputStream.toByteArray()

        return Base64.getEncoder().encodeToString(byteArray)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {

            if (data != null) {
                if (data.clipData != null) {
                    val count: Int = data.clipData!!.itemCount

                    for (i in 0 until count) {
                        val uri: Uri = data.clipData!!.getItemAt(i).uri
                        val bitmap = imageToBitmap(uri)
                        // val base64 = bitmapToBase64(bitmap)

                        daftarImageBmp?.add(bitmap)
                    }

                    viewModel.setDaftarImageBmp(daftarImageBmp!!)
                } else {
                    val bitmap = imageToBitmap(data.data!!)
                    // val base64 = bitmapToBase64(bitmap)

                    daftarImageBmp?.add(bitmap)
                    viewModel.setDaftarImageBmp(daftarImageBmp!!)
                }
            }
        }
    }

}