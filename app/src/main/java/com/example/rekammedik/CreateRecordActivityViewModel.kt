package com.example.rekammedik

import android.content.ContentValues.TAG
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.fragment.app.activityViewModels
import com.example.rekammedik.Constants.COLLECTIONS_NAME
import com.example.rekammedik.Constants.PIMAGES
import com.example.rekammedik.Constants.PNAMA
import com.example.rekammedik.Constants.PREKAM_MEDIS
import com.example.rekammedik.Constants.PSTATUS
import com.example.rekammedik.Constants.PTANGGAL
import com.example.rekammedik.Constants.PUSIA
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlin.math.log


class CreateRecordActivityViewModel : ViewModel() {

    private var databaseFirestore: FirebaseFirestore? = null

    val daftarImageBmp: MutableLiveData<ArrayList<Bitmap>> by lazy {
        MutableLiveData<ArrayList<Bitmap>>()
    }

    val detailRekamMedis: MutableLiveData<RekamMedisData> by lazy {
        MutableLiveData<RekamMedisData>()
    }

    fun getDetailRekamMedis(): LiveData<RekamMedisData> {
        return detailRekamMedis
    }

    fun setDetailRekamMedis(data: RekamMedisData) {
        detailRekamMedis.postValue(data)
    }

    fun getDaftarImageBmp(): LiveData<ArrayList<Bitmap>> {
        return daftarImageBmp
    }

    fun setDaftarImageBmp(daftarImageUriS: ArrayList<Bitmap>) {
        if (daftarImageUriS.isNotEmpty()) {
            daftarImageBmp.postValue(daftarImageUriS)
        }
    }

    fun loadDetailDocuments(documentId: String) {
        databaseFirestore = Firebase.firestore

        databaseFirestore?.let { it ->

            it
                .collection(COLLECTIONS_NAME)
                .document(documentId)
                .get()
                .addOnSuccessListener {
                    val images = it.get(PIMAGES) as ArrayList<String>

                    val rekamMedisData: RekamMedisData = RekamMedisData(
                        documentId,
                        it.get(PNAMA).toString(),
                        it.get(PUSIA).toString(),
                        it.get(PREKAM_MEDIS).toString(),
                        it.get(PSTATUS).toString(),
                        it.get(PTANGGAL).toString(),
                        images,
                    )

                    setDetailRekamMedis(rekamMedisData)
                }

        }
    }

}