package com.example.rekammedik

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class MyAdapter(
    private val context: Context,
    private val list: List<RekamMedisData>
) : BaseAdapter() {

    private var nama: TextView? = null
    private var usia: TextView? = null
    private var deskripsi: TextView? = null
    private var status: TextView? = null
    private var tanggal: TextView? = null

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(p0: Int): RekamMedisData {
        return list[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var convertView = convertView

        convertView = LayoutInflater.from(context).inflate(R.layout.row_item, parent, false)

        nama = convertView.findViewById(R.id.tv_nama_pasien)
        usia = convertView.findViewById(R.id.tv_usia_pasien)
        status = convertView.findViewById(R.id.tv_status_pasien)
        tanggal = convertView.findViewById(R.id.tv_tanggal_pasien)

        nama?.text = list[position].nama
        usia?.text = list[position].usia + " tahun"
        status?.text = list[position].status
        tanggal?.text = list[position].tanggal

        return convertView
    }


}