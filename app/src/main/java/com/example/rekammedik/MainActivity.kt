package com.example.rekammedik

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.rekammedik.Constants.EMAIL_ADDRESS_PATTERN
import com.example.rekammedik.databinding.ActivityMainBinding
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding? = null
    private var auth: FirebaseAuth? = null
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding?.root
        setContentView(view)

        progressDialog = ProgressDialog(this)

        auth = FirebaseAuth.getInstance();

        binding?.btnLogin?.setOnClickListener {

            progressDialog?.setTitle("Kotlin progress bar")
            progressDialog?.setMessage("Application is loading")
            progressDialog?.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog?.show()
            progressDialog?.setCancelable(false);

            login()
        }
    }

    override fun onStart() {
        super.onStart()
        // to check if user is already login or not
        var currentUser = auth?.currentUser
        if (currentUser != null) {
            toDashboard()
        }
    }

    private fun toDashboard() {
        val intent = Intent(this, DashboardActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun login() {
        if (validation()) {
            val email = binding?.etUsername?.text.toString()
            val password = binding?.etPassword?.text.toString()

            auth.let {
                auth?.signInWithEmailAndPassword(email, password)?.addOnCompleteListener {
                    if (it.isSuccessful) {

                        Toast.makeText(this, "login sukses", Toast.LENGTH_SHORT).show()
                        progressDialog?.dismiss()

                        toDashboard()
                    } else
                        Toast.makeText(
                            this,
                            "login failed " + it.exception?.message,
                            Toast.LENGTH_SHORT
                        ).show()
                }
            }
        }

        // progressDialog?.dismiss()
    }

    private fun validation(): Boolean {
        val username = binding?.etUsername?.text
        val password = binding?.etPassword?.text

        if (username?.trim()!!.isEmpty())
            return false
        if (password?.trim()!!.isEmpty())
            return false

        if (!EMAIL_ADDRESS_PATTERN.matcher(username).matches()) {
            Toast.makeText(this, "email tidak valid", Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }
}