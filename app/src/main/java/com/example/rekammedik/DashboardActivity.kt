package com.example.rekammedik

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.rekammedik.databinding.ActivityDashboardBinding

class DashboardActivity : AppCompatActivity() {

    private var binding: ActivityDashboardBinding? = null
    private val viewModel: DashboardViewModel by viewModels()

    private var listAdapter: MyAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDashboardBinding.inflate(layoutInflater)
        val view = binding?.root

        setContentView(view)
        binding?.fabAdd?.setOnClickListener {
            val intent = Intent(this, CreateRecordActivity::class.java)
            startActivity(intent)
        }

        initViewModel()
    }


    override fun onRestart() {
        Log.d(TAG, "onRestart: executed")
        viewModel.loadDocuments()

        super.onRestart()
    }


    private fun initViewModel() {
        // membuat observer
        val daftarRMedisObserver = Observer<List<RekamMedisData>> {
            bindListview()
        }

        // start observing
        viewModel.daftarRekamMedisData.observe(this, daftarRMedisObserver)
    }

    private fun bindListview() {
        listAdapter = MyAdapter(this, viewModel.getRMedis().value!!)

        binding?.lvItemsRm?.adapter = listAdapter
        binding?.lvItemsRm?.setOnItemClickListener { adapterView, view, position, id ->
            val element = listAdapter!!.getItem(position)

            val intent = Intent(this, CreateRecordActivity::class.java)
            intent.putExtra("ID_ITEM", element.id)
            startActivity(intent)
        }

    }

}