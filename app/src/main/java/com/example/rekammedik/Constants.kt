package com.example.rekammedik

import java.util.regex.Pattern

object Constants {
    val EMAIL_ADDRESS_PATTERN: Pattern = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )

    val COLLECTIONS_NAME = "rekam_medis_collection"

    val PNAMA = "nama"
    val PREKAM_MEDIS = "informasi"
    val PSTATUS = "status"
    val PTANGGAL = "tanggal"
    val PUSIA = "usia"
    val PIMAGES = "images"

    val REQUEST_CODE = 1

    val PICK_IMAGE = 100
}