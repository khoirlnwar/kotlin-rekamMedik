package com.example.rekammedik

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.rekammedik.Constants.COLLECTIONS_NAME
import com.example.rekammedik.Constants.PIMAGES
import com.example.rekammedik.Constants.PNAMA
import com.example.rekammedik.Constants.PREKAM_MEDIS
import com.example.rekammedik.Constants.PSTATUS
import com.example.rekammedik.Constants.PTANGGAL
import com.example.rekammedik.Constants.PUSIA
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class DashboardViewModel : ViewModel() {

    private var dbfs: FirebaseFirestore? = null

    val daftarRekamMedisData: MutableLiveData<List<RekamMedisData>> by lazy {
        MutableLiveData<List<RekamMedisData>>().also {
            loadDocuments()
        }
    }

    fun getRMedis(): LiveData<List<RekamMedisData>> {
        return daftarRekamMedisData
    }

    // Do asynchronous process
    fun loadDocuments() {
        dbfs = Firebase.firestore

        dbfs?.let {

            val daftarRMedis: MutableList<RekamMedisData> = mutableListOf()

            it.collection(COLLECTIONS_NAME)
                .get()
                .addOnSuccessListener {
                    for (doc in it) {
                        val id = doc.reference.id
                        val nama = doc.get(PNAMA).toString()
                        val usia = doc.get(PUSIA).toString()
                        val rekamMedis = doc.get(PREKAM_MEDIS).toString()
                        val status = doc.get(PSTATUS).toString()
                        val tanggal = doc.get(PTANGGAL).toString()

                        val images = doc.get(PIMAGES) as ArrayList<String>

                        Log.d(TAG, "loadDocuments: images len " + images.size)

                        val rekamMedisData = RekamMedisData(
                            id,
                            nama,
                            usia,
                            rekamMedis,
                            status,
                            tanggal,
                            images
                        )

                        daftarRMedis.add(rekamMedisData)
                    }
                    daftarRekamMedisData.postValue(daftarRMedis)
                }
        }
    }
}